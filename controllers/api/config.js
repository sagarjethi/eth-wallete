'use strict';

module.exports = {
	port: process.env.PORT || 3000,
	host: process.env.HOST || "127.0.0.1",
	mongoConfig: {
		uri: ""
	},
	mongoOptions: {

	},
	web3Config: {
		localUrl: "http://0.0.0.0:8000",
		infuraRopstenUrl: "",
		infuraRinkebyUrl: "",
		infureMainUrl: "",
	},
	kueConfig: {
		redis: {
			host: "127.0.0.1",
			port: "6379",

		},
	},
	redisConfig: {
		host: "127.0.0.1",
		port: "6379",
	},
	accounts: {
		transaferAccountPublicKey: '',
		transferAccountPrivateKey: '',
		coldAccountPublicKey: '',
	},
	utils: {
		walletSalt: "6296661279",
		bugSnagApiKey: "",
		etherScanApiKey: "",
		etherScanUrl: "",
		withdrawBackoff: 20000,
		depositBackoff: 20000,
		balanceFactor: 1.18,
		failFactor: 0.02,
		gasFactor: 1.12,
		chainId: 12,
		depositConfirmation: 40,
		depositAttempt: 3,
		withdrawAttempt: 3,
		sweepAttempt: 5,
		notifyAttempt: 10,
		transferAccountBalanceLimit: 50,
		depositDiscoverInterval: '30 */2 * * * *',
		depositConfirmationInterval: '* * * * *',
		withdrawDiscoverInterval: '30 */3 * * * *',
		apiPrefix: '/api/ether',
		depositFreezed: 'deposit_freezed',
		withdrawFreezed: 'withdraw_freezed'
	},
	bnbAbi:[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"withdrawEther","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"unfreeze","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"freezeOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"freeze","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"decimalUnits","type":"uint8"},{"name":"tokenSymbol","type":"string"}],"payable":false,"type":"constructor"},{"payable":true,"type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Freeze","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Unfreeze","type":"event"}]
};
