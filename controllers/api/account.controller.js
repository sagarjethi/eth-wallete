var  constant =require("./helper/constant");
var express = require('express');
var router = express.Router();
var Web3 = require('web3');
var Tx = require('ethereumjs-tx');
var requestify = require('requestify');
var myIP = require('my-ip');

var fs = require('fs');

function debug(str) {
    let now = new Date();
    let fname = "debug_" + now.getFullYear() + "_" + (now.getMonth() + 1) + "_" + now.getDate() + ".txt";
    let buffer = new Buffer(str + "\n");
    if (fs.existsSync("logs/" + fname)) {
        fs.open("logs/" + fname, 'a', function (err, fd) {
            if (err) {
                return console.log('error opening file: ' + err);
            }

            fs.write(fd, buffer, 0, buffer.length, null, function (err) {
                if (err) return console.log('error writing file: ' + err);
                fs.close(fd, function () {
                    // console.log('file written');
                })
            });
        });
    } else {
        fs.writeFile("logs/" + fname, buffer, function (err) {
            if (err) {
                return console.log(err);
            }

            // console.log("The file was saved!");
        });
    }
}

var prod = false;
var ip = myIP();
debug("ip=" + ip);

var qlock = true;

prod = true;

var decimals = 2;

router.get('/create', create);
router.get('/balanceOf', balanceOf);
router.get('/balanceOf/:address', balanceOf);
router.get('/getEtherBalance/:address', getEtherBalance);
router.get('/gettokenprice/:amount/:sender/:gasprice', gettokenprice);
router.get('/balanceOfBlock/:address/:blockNumber', balanceOfBlock);
router.get('/sendEther/:WalletAddress/:WalletPrivateKey/:toAddress/:balance', sendEther);
router.get('/sendToken/:WalletAddress/:WalletPrivateKey/:toAddress/:balance', sendToken);
router.get('/Transfer/:WalletAddress/:WalletPrivateKey/:toAddress/:balance', Transfer);
router.get('/getTokenTransaction/:blockNumber', getTokenTransaction);
router.get('/approved/:uaddress/:oaddress', approved);
router.get('/getQueue', getOueue);
router.get('/getnonce', getnonce);

var SmartEthereumWallet = require('smartethereumwallet');
var provider = prod ? "https://mainnet.infura.io/idreBd9RUeiYarmnqYW1" : "https://kovan.infura.io/kixyOXvZF8wkkqqryELG";
console.log(provider);


var smartEthereumWallet = new SmartEthereumWallet(provider);
var walletController = smartEthereumWallet.WalletController;



var web3 = new Web3(new Web3.providers.HttpProvider(provider));
console.log(provider);
var owner_address = "0x00c5e04176d95a286fcce0e68c683ca0bfec8454";
/*var owner_private = prod ? "d9dc813a2d32d0bf777008654477cbbc7cb57dbb7d88d35906a640edad554472" : "5447cf8b5f0237e1f1543b132ae14a85bb9cfdaf1e57acf957250570e0b90aaa";
console.log(owner_address);
var privateKey = new Buffer(owner_private, 'hex');*/

var treatz_contract_address = prod ? "0xB8c77482e45F1F44dE1745F52C74426C631bDD52" : "0xB8c77482e45F1F44dE1745F52C74426C631bDD52";

var ContractABI =constant.configServer.bnbAbi ;

var treatz_token = new web3.eth.Contract(ContractABI, treatz_contract_address);

var company_fee = 1 * 10 ** decimals;
var stripe_fee = 7; // percent

function create(req, res) {
    var account = web3.eth.accounts.create();
    res.status(200).json(account);
}

async function getnonce(req, res) {
    var nonce = await web3.eth.getTransactionCount(owner_address, "pending");
    res.status(200).json(nonce);
}


function balanceOf(req, res) {
    var address = req.params.address;
    if (address == undefined)
        address = owner_address;

    // debug("Get Balance of " + address);
    treatz_token.methods.balanceOf(address).call(function (err, value) {
        if (err) {
            res.status(404);
        }

        // debug("Balance of " + address + " is " + value);
        res.status(200).json({balance: parseInt(value / 10 ** (decimals - 2)) / 100});
    });
}

async function gettokenprice(req, res) {
    var qamount = req.params.amount;
    var sender = req.params.sender;
    var gasprice = req.params.gasprice;


    var amount = qamount * 10 ** decimals;

    var transfer = treatz_token.methods.transferFromWithFee(sender, owner_address, amount, 1);
    var qgasprice = gasprice / 10 ** 9;

    try {
        var gasamount = await transfer.estimateGas({from: owner_address});
        var eth_value = await requestify.get('https://min-api.cryptocompare.com/data/pricemulti?tsyms=USD&fsyms=ETH');
    } catch (e) {
        qlock = false;
        debug("try catch error");
        debug(JSON.stringify(e));
        return;
    }

    var usdpereth = JSON.parse(eth_value.body).ETH.USD;
    //console.log(usdpereth);
    var fee = Math.ceil(gasamount * qgasprice * usdpereth * 10 ** decimals) + company_fee;
    //console.log(fee);
    res.status(200).json({token: (amount - fee) / 100});
}

function getEtherBalance(req, res) {
    var address = req.params.address;
    if (address == undefined)
        address = owner_address;

    // debug("Get Balance of " + address);
    walletController.checkAccountBalance(address, function (err, value) {
        if (err) {
            res.status(404);
        }
        //console.log(value);
        // debug("Balance of " + address + " is " + value);
        res.status(200).json({balance: value});
    });
}

function balanceOfBlock(req, res) {
    var address = req.params.address;
    var blockNumber = req.params.blockNumber;
    if (address == undefined)
        address = owner_address;

    // debug("Get Balance of " + address);
    treatz_token.methods.balanceOf(address).call({}, blockNumber, function (err, value) {
        if (err) {
            res.status(404);
        }

        // debug("Balance of " + address + " is " + value);
        res.status(200).json({balance: parseInt(value / 10 ** (decimals - 2)) / 100});
    });
}


async function sendEther(req, res) {
    try {
        var WalletAddress = req.params.WalletAddress;
        var WalletPrivateKey = req.params.WalletPrivateKey;
        var toAddress = req.params.toAddress;
        var balance = req.params.balance;
        var txHashData = '';
        //  res.send("asdasd");


        var nonce = await web3.eth.getTransactionCount(WalletAddress);
        var amountToSend = web3.utils.toWei(balance, 'ether'); //$1
        var gasObj = {
            to: toAddress,
            nonce: nonce
        };

        var gasPrice = await web3.eth.getGasPrice();
        var gasLimit = await web3.eth.estimateGas(gasObj);


        const rawTx = {
            nonce: nonce,
            gasPrice: web3.utils.toHex(gasPrice),
            gasLimit: web3.utils.toHex(gasLimit),
            to: toAddress,
            value: web3.utils.toHex(amountToSend),
        };

        const tx = new Tx(rawTx);
        var privKey = new Buffer(WalletPrivateKey, 'hex');
        tx.sign(privKey);
        var serializedTx = tx.serialize();
        //console.log('serializedTx : ' + serializedTx);
        web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (err, txHash) {
            if (!err) {
                txHashData = txHash;
                //console.log(txHash);
                if (txHashData != '') {
                    success = {success: 1, message: 'ether send success', txHashData: txHashData};
                    res.send(success);

                } else {
                    var success = {success: 0, message: 'ether send failed'};
                    res.send(success);
                }
            } else {
                res.send({success: 0, message: err.toString()});
            }
        });
    }
    catch (err) {
        res.send({success: 0, message: err.toString()});
    }


}

async function sendToken(req, res) {
    try {
        const chainList = {
            mainnet: 1,
            morden: 2,
            ropsten: 3,
            rinkeby: 4,
            ubiqMainnet: 8,
            ubiqTestnet: 9,
            rootstockMainnet: 30,
            rootstockTestnet: 31,
            kovan: 42,
            ethereumClassicMainnet: 61,
            ethereumClassicTestnet: 62,
            ewasmTestnet: 66,
            gethPrivateChains: 1337
        };
        var WalletAddress = req.params.WalletAddress;
        var WalletPrivateKey = req.params.WalletPrivateKey;
        var toAddress = req.params.toAddress;
        var balance = req.params.balance;
        var txHashData = '';
        //var count = web3.eth.getTransactionCount(WalletAddress);
        var count = 150;
        var contract = new web3.eth.Contract(constant.configServer.bnbAbi, treatz_contract_address, {from: WalletAddress});
        var tokens = web3.utils.toWei(balance.toString(), 'ether');
        const decimals = web3.utils.toBN(18);
        const tokenAmount = web3.utils.toBN(tokens);
        const tokenAmountHex = web3.utils.toHex(tokenAmount);

        var transfer = contract.methods.transfer(toAddress, tokenAmountHex);
        var encodedABI = transfer.encodeABI();
        var nonce = await web3.eth.getTransactionCount(WalletAddress);


        var gasObj = {
            to: toAddress,
            nonce: nonce,
            data: encodedABI
        };
        var gasPrice = await web3.eth.getGasPrice();
        //  var gasLimit = await web3.eth.estimateGas(gasObj);
        let gasPriceIncrementFactor = 1;
        let gasLimitNew;

        let countTrancation = await web3.eth.getBlockTransactionCount("latest");
        let resultBlock = await web3.eth.getBlock("latest", false, (error, result) => {
            gasLimitNew = Math.ceil(result.gasLimit / countTrancation);
        });

        console.log(countTrancation);
        var rawTransaction = {
            nonce: web3.utils.toHex(nonce),
            gasLimit: web3.utils.toHex(Math.ceil(gasLimitNew * constant.configServer.utils.gasFactor)),
            gasPrice: web3.utils.toHex(Math.ceil(gasPrice * constant.configServer.utils.gasFactor * 1.5 * gasPriceIncrementFactor)),
            to: treatz_contract_address,
            value: "0x0",
            data: encodedABI,
            chainId: await web3.eth.net.getId()
        };
        console.log(rawTransaction);
        var privKey = new Buffer(WalletPrivateKey, 'hex');
        var tx = new Tx(rawTransaction);

        tx.sign(privKey);
        var serializedTx = tx.serialize();


        web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (err, hash) {
            if (err) {
                // console.log('Error 1: '+err.toString());
                res.send({success: 0, message: err.toString()});
            } else {
                if (hash != '') {
                    success = {success: 1, message: 'ether send success', txHashData: hash};
                    res.send(success);
                } else {
                    var success = {success: 0, message: 'ether send failed'};
                    res.send(success);
                }
            }
        });
    }
    catch (e) {
        success = {success: 0, message: 'something worng', error:e};
        res.send(success);
    }

}

async function Transfer(req, res) {

    var WalletAddress = req.params.WalletAddress;
    var WalletPrivateKey = req.params.WalletPrivateKey;
    var toAddress = req.params.toAddress;
    var balance = req.params.balance;
    var txHashData = '';
    var qgasprice = 50 / 10 ** 9;

    var count = web3.eth.getTransactionCount(WalletAddress);
    var contract = web3.eth.contract(ContractABI).at(treatz_contract_address);
    var rawTransaction = {
        nonce: web3.utils.toHex(count),
        gasPrice: web3.utils.toHex(web3.utils.toWei(qgasprice.toFixed(9).toString(), 'ether')),
        gasLimit: web3.utils.toHex(qgaslimit),
        to: treatz_contract_address,
        value: 0,
        data: contract.transfer.getData(toAddress, balance, {from: WalletAddress}),
        from: owner_address
    };

    var privKey = new Buffer(WalletPrivateKey, 'hex');
    var tx = new Tx(rawTransaction);

    tx.sign(privKey);
    var serializedTx = tx.serialize();

    web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function (err, hash) {
        if (err) {
            // console.log('Error 1: '+err.toString());
            res.send({success: 0, message: err.toString()});
        } else {
            if (hash != '') {
                success = {success: 1, message: 'ether send success', txHashData: hash};
                res.send(success);
            } else {
                var success = {success: 0, message: 'ether send failed'};
                res.send(success);
            }
        }
    });
}


function getTokenTransaction(req, res) {
    var blockNumber = req.params.blockNumber;
    //console.log(blockNumber);
    var options = {fromBlock: blockNumber, toBlock: 'latest'};
    try {
        //'Transfer' ||
        treatz_token.getPastEvents('Transfer' || 'allEvents', options, function (err, data) {
            if (err) {
                //console.log(err);
                res.status(200).json(err);
            } else {
                if (!err) {
                    // var Tdata = [];
                    /*for(var i in data){
                        if(data[i].returnValues["_value"] != undefined && (data[i].returnValues["_to"] == address || data[i].returnValues["_from"] == address)){
                            Tdata = data[i];
                        }
                    }*/
                } else {
                    console.log(err);
                }
                res.status(200).json(data);
            }
        });
    } catch (e) {
        res.status(200).json(e);
    }
}


function approved(req, res) {
    var user_address = req.params.uaddress;
    var owner_address = req.params.oaddress;
    if (user_address == undefined || owner_address == undefined)
        res.status(404);

    treatz_token.methods.allowance(user_address, owner_address).call(function (err, value) {
        if (err) {
            return;
        }

        res.status(200).json({err, value});
    });
}

async function getQueue() {
    if (qlock)
        return;
    qlock = true;

    let rows = await mysqlcon.query("SELECT * FROM eth_users_transactions WHERE `confirmed`=0 ORDER BY gasprice DESC, id ASC");
    if (!rows || !rows.length) {
        qlock = false;
        return;
    }

    let qid = rows[0].id;
    let amount = rows[0].amount;
    let tx_from = rows[0].tx_from;
    let tx_to = rows[0].tx_to;
    let qgaslimit = rows[0].gaslimit;
    let qgasprice = rows[0].gasprice / 10 ** 9;
    let qnonce = rows[0].nonce;

    let sender = "";
    let recver = "";

    if (tx_from == "Charge")
        sender = owner_address;
    else {
        let srows = await mysqlcon.query("SELECT * FROM eth_users_addresses WHERE username='" + tx_from + "'");

        if (!srows || !srows.length) {
            await mysqlcon.query("DELETE FROM eth_users_transactions WHERE id=" + qid);
            qlock = false;
            return;
        }
        sender = srows[0].address;
    }

    let rrows = await mysqlcon.query("SELECT * FROM eth_users_addresses WHERE username='" + tx_to + "'");

    if (!rrows || !rrows.length) {
        await mysqlcon.query("DELETE FROM eth_users_transactions WHERE id=" + qid);
        qlock = false;
        return;
    }
    recver = rrows[0].address;

    await mysqlcon.query("UPDATE eth_users_transactions SET confirmed=2 WHERE id=" + qid);

    if (tx_from == "Charge")
        qcharge(recver, amount, qgaslimit, qgasprice, qnonce, qid);
    else
        qsendtrz(sender, recver, amount, qgaslimit, qgasprice, qnonce, qid);
}

// setInterval(getQueue, 1);

async function qcharge(recver, qamount, qgaslimit, qgasprice, qnonce, qid) {
    debug("----------------------- Start charge ----------------------- nonce=" + qnonce);
    var amount = qamount * 10 ** decimals;
    var transfer = treatz_token.methods.transfer(recver, amount);
    try {
        var nonce = qnonce;
        if (!nonce)
            nonce = await web3.eth.getTransactionCount(owner_address, "pending");
        var gasamount = await transfer.estimateGas({from: owner_address});
        var eth_value = await requestify.get('https://min-api.cryptocompare.com/data/pricemulti?tsyms=USD&fsyms=ETH');
    } catch (e) {
        qlock = false;
        debug("try catch error");
        debug(JSON.stringify(e));
        await mysqlcon.query("UPDATE eth_users_transactions SET confirmed=0 WHERE id=" + qid);
        return;
    }

    var usdpereth = JSON.parse(eth_value.body).ETH.USD;
    // var fee = Math.ceil(gasamount * gasprice * usdpereth * 10 ** decimals) + company_fee;
    var fee = Math.ceil(gasamount * qgasprice * usdpereth * 10 ** decimals) + company_fee + Math.ceil(amount * stripe_fee * 0.01);

    debug("usd per eth, gas amount, fee, amount, nonce");
    debug(usdpereth + ', ' + gasamount + ', ' + fee + ', ' + amount + ', ' + nonce);

    transfer = treatz_token.methods.transfer(recver, amount - fee);

    process_transfer(transfer, nonce, qgaslimit, qgasprice, qid, amount - fee);
}

async function qsendtrz(sender, recver, qamount, qgaslimit, qgasprice, qnonce, qid) {
    debug("----------------------- Start send ----------------------- nonce=" + qnonce);
    var amount = qamount * 10 ** decimals;
    var transfer = treatz_token.methods.transferFromWithFee(sender, recver, amount, 1);
    try {
        var nonce = qnonce;
        if (!nonce)
            nonce = await web3.eth.getTransactionCount(owner_address, "pending");
        var gasamount = await transfer.estimateGas({from: owner_address});
    } catch (e) {
        qlock = false;
        debug("try catch error");
        debug(JSON.stringify(e));
        return;
    }

    // var fee = Math.ceil(gasamount * gasprice * usdpereth * 10 ** decimals) + company_fee;
    var fee = Math.ceil(gasamount * qgasprice * qamount * 10 ** decimals) + company_fee;

    debug("usd per eth, gas amount, fee, amount, nonce");
    debug(usdpereth + ', ' + gasamount + ', ' + fee + ', ' + amount + ', ' + nonce);

    transfer = treatz_token.methods.transferFromWithFee(sender, recver, amount - fee, fee);

    process_transfer(transfer, nonce, qgaslimit, qgasprice, qid, amount - fee);
}

function process_transfer(transfer, nonce, qgaslimit, qgasprice, qid, summary) {
    debug("==============process_transfer============== nonce=" + nonce);
    debug("nonce, qgaslimit, qgasprice");
    debug(nonce + ', ' + qgaslimit + ', ' + qgasprice.toFixed(9));
    var encodedABI = transfer.encodeABI();

    var tra = {
        nonce: web3.utils.toHex(nonce),
        gasPrice: web3.utils.toHex(web3.utils.toWei(qgasprice.toFixed(9).toString(), 'ether')),
        gasLimit: web3.utils.toHex(qgaslimit),
        to: treatz_contract_address,
        value: 0,
        data: encodedABI,
        from: owner_address
    };

    var tx = new Tx(tra);
    tx.sign(privateKey);

    var stx = tx.serialize();
    var tran = web3.eth.sendSignedTransaction('0x' + stx.toString('hex'));

    tran.on('confirmation', (confirmationNumber, receipt) => {
        //debug('confirmation', confirmationNumber, receipt);
    });

    tran.on('transactionHash', (hash) => {
        debug('transactionHash= ' + hash);
        sendResultToWallet(qid, "sethash", hash, nonce, summary);
        qlock = false;
    });
    tran.on('receipt', receipt => {
        debug('send receipt= \n' + JSON.stringify(receipt));

        if (receipt.logs.length == 0)
            sendResultToWallet(qid, "fail", receipt.transactionHash, nonce);
        else
            sendResultToWallet(qid, "success", receipt.transactionHash, nonce);
    });
    tran.on('error', (error) => {
        debug('transaction error\n' + error);
        qlock = false;
    });
}

function sendResultToWallet(id, result, hash, nonce, summary) {
    debug("====================sendResultToWallet====================");
    debug("id, result, hash, nonce, summary");
    debug(id + ', ' + result + ', ' + hash + ', ' + nonce + ', ' + summary);
    if (result == "success") {
        mysqlcon.query("UPDATE eth_users_transactions SET confirmed=1, confirm_date=now() WHERE id=" + id);
    } else if (result == "sethash") {
        mysqlcon.query("UPDATE eth_users_transactions SET hash='" + hash + "', tx_date=now(), nonce=" + nonce + ", summary=" + summary + " WHERE id=" + id);
    } else if (result == "fail") {
        mysqlcon.query("UPDATE eth_users_transactions SET confirmed=0, hash='' WHERE id=" + id);
    }
}

async function getStatus() {
    setTimeout(getStatus, 60 * 1000);

    let rows = await mysqlcon.query("SELECT * FROM eth_users_transactions WHERE confirmed=2 ORDER BY gasprice DESC, id ASC");
    if (!rows || !rows.length) {
        return;
    }

    for (let i = 0; i < rows.length; i++) {
        if (rows[i].hash == '') {
            let now = new Date();
            let txd = rows[i].tx_date;
            let tx_date = txd.getFullYear() + '-' + (txd.getMonth() < 9 ? '0' + (txd.getMonth() + 1) : txd.getMonth() + 1) + '-' + (txd.getDate() < 10 ? '0' + txd.getDate() : txd.getDate());
            let now_date = now.getFullYear() + '-' + (now.getMonth() < 9 ? '0' + (now.getMonth() + 1) : now.getMonth() + 1) + '-' + (now.getDate() < 10 ? '0' + now.getDate() : now.getDate());
            if (tx_date != now_date) {
                await mysqlcon.query("UPDATE eth_users_transactions SET confirmed=0 WHERE id=" + rows[i].id);
            }
            continue;
        }
        if (rows[i].hash != '')
            getTransactionStatus(rows[i].id, rows[i].hash, rows[i].nonce);
    }
}

async function getTransactionStatus(id, hash, nonce) {
    var transaction = web3.eth.getTransaction(hash, function (error, tx) {
        if (error) {
            debug(JSON.stringify(error));
            return;
        }

        if (tx == null || tx == undefined) {
            debug(' > transaction ' + hash + ' is not exist');
            sendResultToWallet(id, "fail", hash, nonce);
        } else {
            debug(' > transaction ' + hash);
            debug('tx=' + JSON.stringify(tx));

            if (tx.blockHash == null || tx.blockNumber == null || tx.transactionIndex == null) {
                debug(' > transaction ' + hash + ' is pending');
                return;
            }

            web3.eth.getTransactionReceipt(hash, function (error, tx) {
                if (error) {
                    debug(JSON.stringify(error));
                    return;
                }

                if (tx == null || tx == undefined) {
                    debug(' > transaction ' + hash + ' is no receipted');
                    return;
                }
                debug(' > transaction ' + hash);
                debug('tx=' + JSON.stringify(tx));

                if (tx.blockHash == null || tx.blockNumber == null || tx.transactionIndex == null || tx.logs == null || tx.logs == undefined)
                    return;

                if (tx.logs.length == 0) {
                    sendResultToWallet(id, "fail", hash, nonce);
                    return;
                }

                sendResultToWallet(id, "success", hash, nonce);
            });
        }
    });
}


async function getStatusEth() {
    //console.log('call eth');
    setTimeout(getStatusEth, 60 * 1000);

    let rows = await mysqlcon.query("SELECT * FROM eth_transactions WHERE confirmed=0 AND tx_date < ((now() - INTERVAL 3 MINUTE)) ORDER BY id ASC");
    if (!rows || !rows.length) {
        return;
    }

    for (let i = 0; i < rows.length; i++) {
        /*if (rows[i].hash == '')
        {
            let now = new Date();
            let txd = rows[i].tx_date;
            let tx_date = txd.getFullYear() + '-' + (txd.getMonth() < 9 ? '0' + (txd.getMonth() + 1) : txd.getMonth() + 1) + '-' + (txd.getDate() < 10 ? '0' + txd.getDate() : txd.getDate());
            let now_date = now.getFullYear() + '-' + (now.getMonth() < 9 ? '0' + (now.getMonth() + 1) : now.getMonth() + 1) + '-' + (now.getDate() < 10 ? '0' + now.getDate() : now.getDate());
            if (tx_date != now_date)
            {
                await mysqlcon.query("UPDATE eth_users_transactions SET confirmed=0 WHERE id=" + rows[i].id);
            }
            continue;
        }*/
        if (rows[i].hash != '')
        //console.log('call eth status');
            getEthTransactionStatus(rows[i].id, rows[i].hash, rows[i].nonce);
    }
}

async function getEthTransactionStatus(id, hash, nonce) {
    var transaction = web3.eth.getTransaction(hash, function (error, tx) {
        if (error) {
            debug(JSON.stringify(error));
            return;
        }

        if (tx == null || tx == undefined) {
            debug(' > transaction ' + hash + ' is not exist');
            sendEthResultToWallet(id, "fail", hash, nonce);
        } else {
            debug(' > transaction ' + hash);
            debug('tx=' + JSON.stringify(tx));

            if (tx.blockHash == null || tx.blockNumber == null || tx.transactionIndex == null) {
                debug(' > transaction ' + hash + ' is pending');
                return;
            }

            web3.eth.getTransactionReceipt(hash, function (error, tx) {
                if (error) {
                    debug(JSON.stringify(error));
                    return;
                }

                if (tx == null || tx == undefined) {
                    debug(' > transaction ' + hash + ' is no receipted');
                    return;
                }
                debug(' > transaction ' + hash);
                debug('tx=' + JSON.stringify(tx));

                if (tx.blockHash == null || tx.blockNumber == null || tx.transactionIndex == null || tx.logs == null || tx.logs == undefined)
                    return;

                if (tx.status == '0x0') {
                    sendEthResultToWallet(id, "fail", hash, nonce);
                    return;
                }

                sendEthResultToWallet(id, "success", hash, nonce);
            });
        }
    });
}


function sendEthResultToWallet(id, result, hash, nonce, summary) {
    debug("====================sendResultToWallet====================");
    debug("id, result, hash, nonce, summary");
    debug(id + ', ' + result + ', ' + hash + ', ' + nonce + ', ' + summary);
    if (result == "success") {
        mysqlcon.query("UPDATE eth_transactions SET confirmed=1, confirm_date=now() WHERE id=" + id);
    } else if (result == "sethash") {
        mysqlcon.query("UPDATE eth_transactions SET hash='" + hash + "', tx_date=now(), nonce=" + nonce + ", summary=" + summary + " WHERE id=" + id);
    } else if (result == "fail") {
        //mysqlcon.query("UPDATE eth_transactions SET confirmed=0, hash='' WHERE id=" + id);
    }
}

async function getOueue(req, res) {
    var system_info = {};
    system_info.owner_address = owner_address;
    system_info.owner_private = owner_private;
    system_info.contract_address = treatz_contract_address;
    system_info.contract_abi = ContractABI;

    res.status(200).json(system_info);
}

module.exports = router;
