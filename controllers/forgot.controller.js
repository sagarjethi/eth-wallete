﻿var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');
var userService = require('services/user.service');
var md5 = require('md5');
const nodemailer = require("nodemailer");

router.get('/', function (req, res) {
    // log user out
    delete req.session.token;

    // move success message into local variable so it only appears once (single read)
    var viewData = { success: req.session.success, action: 'forgot' };
    delete req.session.success;

    res.render('forgot', viewData);
});

router.post('/', function (req, res) {
    userService.getByEmail(req.body.useremail)
        .then(function (user) {
            if (user) {
                var nowDate = new Date();
                var nowDateStr = (nowDate.getFullYear()).toString();
                if (nowDate.getMonth() < 9)
                  nowDateStr += "0" + (nowDate.getMonth() + 1).toString();
                else
                  nowDateStr += (nowDate.getMonth() + 1).toString();
                if (nowDate.getDate() < 10)
                  nowDateStr += "0" + (nowDate.getDate()).toString();
                else
                  nowDateStr += (nowDate.getDate()).toString();
                var hashtoken = md5(user.useremail + "_" + nowDateStr);
                userService.updateResetToken(user._id, hashtoken)
                .then(function(){
                  // Send email
                  let transporter = nodemailer.createTransport({
                      host: 'smtp.gmail.com',
                      port: 465,
                      secure: true, // true for 465, false for other ports
                      auth: {
                          user: "juliafodor828@gmail.com", // generated ethereal user
                          pass: "aqryqocynvsbxoux"  // generated ethereal password
                      }
                  });

                  let subject = "";
                  let c_text = "";
                  let c_html = "";
                  subject = "Reset password";
                  c_html = '<b>Hi ' + user.firstName + "</b><br><br>Click <a href='http://localhost:3000/reset/" + hashtoken + "' target='_blank'>here</a> to reset password.";
                  c_text = 'Hi ' + user.firstName + "\n\nClick http://localhost:3000/reset/" + hashtoken + " to reset password.";

                  // setup email data with unicode symbols
                  let mailOptions = {
                      from: '"ICP manager" < support@icp-analysis.com >', // sender address
                      to: user.useremail, // list of receivers
                      subject: subject, // Subject line
                      text: c_text, // plain text body
                      html: c_html // html body
                  };
                  // send mail with defined transport object
                  transporter.sendMail(mailOptions, (error, info) => {
                      if (error) {
                        return res.render('forgot', { error: 'An error occurred', action: 'forgot' });
                      }
                      console.log('Message sent: %s', info.messageId);
                      // Preview only available when sending through an Ethereal account
                      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                      return res.render('forgot', { success: 'Check your email.', action: 'forgot' });
                  });
                })
                .catch(function(){
                  return res.render('forgot', { error: 'An error occurred', action: 'forgot' });
                });
            } else {
                console.log("no exists user");
                return res.render('forgot', { error: 'Email is not exists.', action: 'forgot' });
            }
        })
        .catch(function (err) {
            console.log("a error", err);
            return res.render('forgot', { error: 'An error occurred', action: 'forgot' });
        });
});

module.exports = router;
