﻿var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');
var userService = require('services/user.service');

router.get('/:resetToken', function (req, res) {
    var resetToken = req.params.resetToken;
    userService.getByToken(resetToken)
        .then(function (user) {
            if (user) {
                res.render('reset', {action: 'signin', userId: user._id});
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
});

router.post('/:resetToken', function (req, res) {
    userService.getById(req.body.userId)
        .then(function (user) {
            if (user) {
                userService.updatePassword(user._id, req.body.password)
                .then(function(){
                  return res.render('reset', { success: 'Password is reseted.', action: 'signin', userId: req.body.userId });
                })
                .catch(function(){
                  return res.render('reset', { error: 'An error occurred1', action: 'signin', userId: req.body.userId });
                });
            } else {
                return res.render('reset', { error: 'User is not exists.', action: 'signin', userId: req.body.userId });
            }
        })
        .catch(function (err) {
            return res.render('reset', { error: 'An error occurred2', action: 'signin', userId: req.body.userId });
        });
});

module.exports = router;
