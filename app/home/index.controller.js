﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Home.IndexController', Controller);

    function Controller($rootScope, AquariumService, TestService, ElementService, FlashService, LocationService, $timeout) {
        var vm = this;
        vm.elements = [];
        vm.aquariums = [];
        vm.tests = [];
        vm.test = {};
        vm.recently_elems = [];

        vm.isContain = isContain;
        vm.add_aquarium = add_aquarium;
        vm.show_step_1_form = show_step_1_form;
        vm.units_toggle = units_toggle;
        vm.add_new_aquarium = add_new_aquarium;
        vm.open_aquarium = open_aquarium;
        vm.purchase_aquarium = purchase_aquarium;
        vm.add_new_test = add_new_test;
        vm.update_test_status = update_test_status;
        vm.start_new_test = start_new_test;
        vm.show_element_result = show_element_result;
        vm.select_location = select_location;

        vm.view_aquarium = false;
        vm.new_aquarium = false;
        vm.show_step_1 = true;
        vm.show_step_1_content = true;
        vm.aquarium_step_2 = false;
        vm.aquarium_step_3 = false;
        vm.aquarium = null;

        vm.locations = [];
        vm.location_id = '';
        vm.element_id = '';

        initController();

        function initController() {
          $timeout(function(){
            $rootScope.flash = false;
          }, 3000);

          if (!$rootScope.curuser)
          {
            $timeout(initController, 100);
            return;
          }
          vm.aquarium = {user_id: $rootScope.curuser._id, units: 'g', purchased: 0};
          // get aquariums list
          AquariumService.GetByUser($rootScope.curuser._id).then(function (aquariums) {
              vm.aquariums = Object.values(aquariums);
              if (vm.aquariums.length > 0)
              {
                open_aquarium(aquariums[0]._id);
              }
          });

          // get elements list
          ElementService.GetAll().then(function (elements) {
              vm.elements = Object.values(elements);
          });

          LocationService.GetAll().then(function (locations) {
            vm.locations = Object.values(locations);
            if (vm.locations.length > 0)
              vm.location_id = vm.locations[0]._id;
          });

          vm.view_aquarium = false;
          vm.new_aquarium = true;
        }

        function select_location(){
          for(var i = 0; i < vm.locations.length; i++)
          {
            if (vm.locations[i]._id == vm.location_id)
            {
              for(var j = 0; j < vm.locations[i].values.length; j++)
              {
                if (vm.element_id == vm.locations[i].values[j].element_id)
                {
                  $(".elem-measure-compare-value").text(vm.locations[i].values[j].value);
                  var h = parseFloat(vm.locations[i].values[j].value) * 120 / 2;
                  if (h == 0) h = 1;
                  $(".dashboard-graph-compare-item").css("height", h + "px");
                  break;
                }
              }
              break;
            }
          }
        }

        function isContain(elem) {
          return vm.recently_elems.indexOf(elem) != -1;
        }

        function add_aquarium() {
          vm.view_aquarium = false;
          vm.new_aquarium = true;
          vm.aquarium_step_2 = false;
          vm.aquarium_step_3 = false;
          vm.aquarium = {user_id: $rootScope.curuser._id, units: 'g', purchased: 0};

          $(".users-nav .active").removeClass("active");
          $("#add_aquarium_nav").addClass("active");
        }

        function show_step_1_form() {
          vm.show_step_1 = true;
          vm.show_step_1_content = true;
        }

        function units_toggle() {
          if ($(".units-toggle-btn").hasClass("units-toggle-btn-g"))
          {
            $(".units-toggle-btn").removeClass("units-toggle-btn-g");
            $(".units-toggle-btn").addClass("units-toggle-btn-l");
            vm.aquarium.units = 'L';
          } else {
            $(".units-toggle-btn").removeClass("units-toggle-btn-l");
            $(".units-toggle-btn").addClass("units-toggle-btn-g");
            vm.aquarium.units = 'g';
          }
        }
        function add_new_aquarium() {
          if (vm.aquarium.tankname == undefined || vm.aquarium.tankname.trim() == "")
          {
            $('#tankname').focus();
            return;
          } else if (vm.aquarium.volume == undefined || vm.aquarium.volume.trim() == "") {
            $('#volume').focus();
            return;
          }
          AquariumService.Create(vm.aquarium)
          .then(function(result){
            vm.aquariums.push(result.ops[0]);
            open_aquarium(result.ops[0]._id);
          })
          .catch(function(error){
            FlashService.Error(error);

            $timeout(function(){
              $rootScope.flash = false;
            }, 2000);
          });
        }
        function open_aquarium(aquarium_id) {
          if (aquarium_id === "")
          {
            return;
          }
          vm.tests = [];
          AquariumService.GetById(aquarium_id)
          .then(function (aquarium) {
            vm.aquarium = aquarium;
            vm.aquarium_id = aquarium._id;
            $(".users-nav .active").removeClass("active");
            $("#aquarium-li-" + aquarium_id).addClass("active");
            vm.new_aquarium = false;
            vm.view_aquarium = false;
            vm.aquarium_step_2 = false;
            vm.aquarium_step_3 = false;
            if (parseInt(vm.aquarium.purchased) !== 1)
            {
              vm.aquarium_step_2 = true;
            }
            else
            {
              TestService.GetByAquariumWithResults(vm.aquarium._id).then(function(tests){
                vm.tests = Object.values(tests);
                var sent_cnt = 0;
                for(var i = 0; i < vm.tests.length; i++)
                {
                  if (vm.tests[i].status != 0)
                    sent_cnt++;
                }
                if (sent_cnt === 0)
                {
                  vm.aquarium_step_3 = true;
                  if (vm.tests.length == 0)
                    get_unique_test();
                  else
                  {
                    let unique_id = vm.tests[0].unique_id;
                    vm.test = vm.tests[0];
                    $("#unique_id").val(unique_id.substring(0, 3) + '-' + unique_id.substring(3, 6) + '-' + unique_id.substring(6, 9));
                  }
                }
                else
                {
                  vm.view_aquarium = true;
                  vm.recently_elems = [];
                  for(var i = 0; i < vm.elements.length; i++)
                  {
                    for(var j = vm.tests.length - 1; j >= 0; j--)
                    {
                      if (vm.tests[j].status == 2)
                      {
                        for(var k = 0; k < vm.tests[j].results.length; k++)
                        {
                          if (vm.elements[i].symbol == vm.tests[j].results[k].element.symbol)
                          {
                            if (vm.elements[i].elemLow > vm.tests[j].results[k].value || vm.elements[i].elemHigh < vm.tests[j].results[k].value)
                            {
                              vm.recently_elems.push(vm.elements[i].symbol);
                            }
                            break;
                          }
                        }
                        break;
                      }
                    }
                  }
                }
                  
                show_element_result(vm.elements[0]);
              });
            }
          });
        }
        var cur_element = null;
        function show_element_result(element)
        {
          vm.element_id = element._id;
          cur_element = element;
          if (element.symbol == undefined)
            element.symbol = "";
          if (element.elemName == undefined)
            element.elemName = "";
          if (element.elemDesc == undefined)
            element.elemDesc = "";
          $(".element-item").removeClass("active");
          $(".element-item-" + element.symbol).addClass("active");
          $(".elem-symbol").text(element.symbol);
          $(".elem-name").text(element.elemName);
          $(".elem-desc").text(element.elemDesc);
          var elem_measure_value = 0;
          for(var i = vm.tests.length - 1; i >= 0; i--)
          {
            if (vm.tests[i].results != undefined && vm.tests[i].results.length > 0)
            {
              var find_elem = false;
              for (var j = 0; j < vm.tests[i].results.length; j++)
              {
                if (element.symbol == vm.tests[i].results[j].element.symbol)
                {
                  elem_measure_value = vm.tests[i].results[j].value;
                  find_elem = true;
                  break;
                }
              }
              if (find_elem)
                break;
            }
          }
          $(".elem-measure-current-value").text(elem_measure_value);
          for(var i = 0; i < vm.elements.length; i++)
          {
            if (vm.elements[i].symbol == element.symbol)
            {
              if (elem_measure_value > vm.elements[i].elemHigh)
              {
                $(".elem-test-desc").text(vm.elements[i].adviceHigh);
                $(".elem-measure-type").text("High");
              }
              else if (elem_measure_value < vm.elements[i].elemLow)
              {
                $(".elem-test-desc").text(vm.elements[i].adviceLow);
                $(".elem-measure-type").text("Low");
              }
              else
              {
                $(".elem-test-desc").text("");
                $(".elem-measure-type").text("");
              }
              break;
            }
          }
          var graphs_html = '';
          var graphs_cnt = vm.tests.length + 1;
          var scroll_graph_width = $("main").width();
          var last_value_id = 1;
          scroll_graph_width = (scroll_graph_width - 215) * 0.8 - 40;

          for(var i = 0; i < vm.tests.length; i++)
          {
            var item_value_height = 60;
            var item_value_txt = "Awaiting Test Results";
            var item_value_cls = " awaiting";
            var item_value_date = vm.tests[i].test_date;
            if (vm.tests[i].results != undefined && vm.tests[i].results.length > 0)
            {
              for (var j = 0; j < vm.tests[i].results.length; j++)
              {
                if (element.symbol == vm.tests[i].results[j].element.symbol)
                {
                  var measure_value = parseFloat(vm.tests[i].results[j].value);
                  item_value_height = measure_value * 120 / 2;
                  if (item_value_height == 0) item_value_height = 1;
                  last_value_id = i + 1;
                  item_value_txt = '';
                  item_value_cls = '';
                  break;
                }
              }
            }
            if (scroll_graph_width / graphs_cnt > 100)
              graphs_html += '<div class="dashboard-graph-item" style="left: ' + (100 * i / graphs_cnt) + '%; width: ' + (100 / graphs_cnt) + '%;"><div class="dashboard-graph-value' + item_value_cls + '" style="height:' + item_value_height + 'px;">' + item_value_txt + '</div><div class="dashboard-graph-date">' + item_value_date + '</div></div>';
            else
              graphs_html += '<div class="dashboard-graph-item" style="left: ' + (i * 100) + 'px; width: 100px;"><div class="dashboard-graph-value' + item_value_cls + '" style="height:' + item_value_height + 'px;">' + item_value_txt + '</div><div class="dashboard-graph-date">' + item_value_date + '</div></div>';
          }
          if (scroll_graph_width / graphs_cnt > 100)
            $(".dashboard-graph-newtest").css({left: (100 * i / graphs_cnt) + "%", width: (100 / graphs_cnt) + "%"});
          else
            $(".dashboard-graph-newtest").css({left: (100 * i) + "px", width: "100px"});

          if (scroll_graph_width / graphs_cnt > 100)
            $("#dashboard-graph-scroll").removeClass("scrolling");
          else
            $("#dashboard-graph-scroll").addClass("scrolling");

          $(".dashbaord-graph-board").html(graphs_html);
          $(".dashboard-graph-item:nth-child(" + last_value_id + ") .dashboard-graph-value").addClass("highlight");
          select_location();
        }
        function purchase_aquarium() {
          AquariumService.Update({_id: vm.aquarium._id, purchased: 1})
          .then(function(result){
            open_aquarium(vm.aquarium._id);
          })
          .catch(function(error){
            FlashService.Error(error);

            $timeout(function(){
              $rootScope.flash = false;
            }, 2000);
          });
        }
        function get_unique_test() {
          if (vm.tests.unique_id)
            return;
          let unique_id = 0;
          while(1)
          {
            unique_id = parseInt((Math.random() * 9 / 10 + 0.1) * 1000000000);
            if (unique_id > 100000000 && unique_id < 1000000000)
              break;
          }
          TestService.GetByUnique(unique_id)
          .then(function(result){
            if (result._id !== undefined)
            {
              get_unique_test();
            }
            else
            {
              unique_id = unique_id + "";
              $("#unique_id").val(unique_id.substring(0, 3) + '-' + unique_id.substring(3, 6) + '-' + unique_id.substring(6, 9));
              add_new_test();
            }
          })
          .catch(function(error){
            console.log(error);
          });
        }
        function start_new_test(){
          vm.aquarium_step_3 = true;
          vm.view_aquarium = false;

          for(var i = 0; i < vm.tests.length; i++)
          {
            if (vm.tests[i].status == 0)
            {
              let unique_id = vm.tests[i].unique_id;
              vm.test = vm.tests[i];
              $("#unique_id").val(unique_id.substring(0, 3) + '-' + unique_id.substring(3, 6) + '-' + unique_id.substring(6, 9));
              return;
            }
          }
          get_unique_test();
        }
        function addZero(i)
        {
          if (i < 10)
            return "0" + i;
          return i;
        }
        function add_new_test() {
          if ($("#unique_id").val() == "")
          {
            return;
          }

          let today = new Date();
          let test_date = addZero(today.getMonth() + 1) + "-" + addZero(today.getDate()) + '-' + today.getFullYear();
          let test_time = addZero(today.getHours()) + ":" + addZero(today.getMinutes());

          let unique_id = $("#unique_id").val();
          unique_id = unique_id.replace(/-/g, '');

          TestService.Create({aquarium_id: vm.aquarium._id, unique_id: unique_id, test_date: test_date, test_time: test_time, status: 0})
          .then(function(result){
            vm.test = result.ops[0];
            vm.tests.push(result.ops[0]);
            //open_aquarium(vm.aquarium._id);
            //FlashService.Success('Success');
            //initController();
          })
          .catch(function(error){
            FlashService.Error(error);

            $timeout(function(){
              $rootScope.flash = false;
            }, 2000);
          });
        }
        function update_test_status()
        {
          let today = new Date();
          let sent_date = addZero(today.getMonth() + 1) + "-" + addZero(today.getDate()) + '-' + today.getFullYear();
          let sent_time = addZero(today.getHours()) + ":" + addZero(today.getMinutes());

          let unique_id = $("#unique_id").val();
          unique_id = unique_id.replace(/-/g, '');

          vm.test.sent_date = sent_date;
          vm.test.sent_time = sent_time;
          vm.test.status = 1;

          TestService.Update(vm.test)
          .then(function(result){
            open_aquarium(vm.aquarium._id);
            //FlashService.Success('Success');
            //initController();
          })
          .catch(function(error){
            FlashService.Error(error);

            $timeout(function(){
              $rootScope.flash = false;
            }, 2000);
          });
        }
    }

})();
