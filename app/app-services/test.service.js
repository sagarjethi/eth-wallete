﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('TestService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByAquarium = GetByAquarium;
        service.GetByUnique = GetByUnique;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.UpdateUnique = UpdateUnique;
        service.GetByAquariumWithResults = GetByAquariumWithResults;

        return service;

        function GetAll() {
            return $http.get('/api/tests').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/tests/' + _id).then(handleSuccess, handleError);
        }

        function GetByAquarium(aquarium_id) {
            return $http.get('/api/tests/aquarium/' + aquarium_id).then(handleSuccess, handleError);
        }

        function GetByAquariumWithResults(aquarium_id) {
            return $http.get('/api/tests/aquarium-with-results/' + aquarium_id).then(handleSuccess, handleError);
        }

        function GetByUnique(unique_id) {
            return $http.get('/api/tests/unique/' + unique_id).then(handleSuccess, handleError);
        }

        function Create(test) {
            return $http.post('/api/tests', test).then(handleSuccess, handleError);
        }

        function UpdateUnique(test) {
            return $http.put('/api/tests/unique/' + test._id, test).then(handleSuccess, handleError);
        }

        function Update(test) {
            return $http.put('/api/tests/' + test._id, test).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/tests/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
