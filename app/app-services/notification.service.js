﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('NotificationService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetCurrent = GetCurrent;
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetCurrent() {
            return $http.get('/api/notifications/current').then(handleSuccess, handleError);
        }

        function GetAll() {
            return $http.get('/api/notifications').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/notifications/' + _id).then(handleSuccess, handleError);
        }

        function Create(notification) {
            return $http.post('/api/notifications', notification).then(handleSuccess, handleError);
        }

        function Update(notification) {
            return $http.put('/api/notifications/' + notification._id, notification).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/notifications/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
