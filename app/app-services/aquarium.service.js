﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AquariumService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetCurrent = GetCurrent;
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUser = GetByUser;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetCurrent() {
            return $http.get('/api/aquariums/current').then(handleSuccess, handleError);
        }

        function GetAll() {
            return $http.get('/api/aquariums').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/aquariums/' + _id).then(handleSuccess, handleError);
        }

        function GetByUser(user_id) {
            return $http.get('/api/aquariums/user/' + user_id).then(handleSuccess, handleError);
        }

        function Create(aquarium) {
            return $http.post('/api/aquariums', aquarium).then(handleSuccess, handleError);
        }

        function Update(aquarium) {
            return $http.put('/api/aquariums/' + aquarium._id, aquarium).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/aquariums/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
