﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('ElementService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetCurrent = GetCurrent;
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetCurrent() {
            return $http.get('/api/elements/current').then(handleSuccess, handleError);
        }

        function GetAll() {
            return $http.get('/api/elements').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/elements/' + _id).then(handleSuccess, handleError);
        }

        function Create(element) {
            return $http.post('/api/elements', element).then(handleSuccess, handleError);
        }

        function Update(element) {
            return $http.put('/api/elements/' + element._id, element).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/elements/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
