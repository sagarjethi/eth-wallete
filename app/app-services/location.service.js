﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('LocationService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetCurrent = GetCurrent;
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetCurrent() {
            return $http.get('/api/locations/current').then(handleSuccess, handleError);
        }

        function GetAll() {
            return $http.get('/api/locations').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/locations/' + _id).then(handleSuccess, handleError);
        }

        function Create(location) {
            return $http.post('/api/locations', location).then(handleSuccess, handleError);
        }

        function Update(location) {
            return $http.put('/api/locations/' + location._id, location).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/locations/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
