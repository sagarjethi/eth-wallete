﻿(function () {
    'use strict';

    angular
        .module('app', ['ui.router'])
        .config(config)
        .run(run);

    function config($stateProvider, $urlRouterProvider) {
        // default route
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'home/index.html',
                controller: 'Home.IndexController',
                controllerAs: 'vm',
                data: { activeTab: 'home' }
            })
            .state('account', {
                url: '/account',
                templateUrl: 'account/index.html',
                controller: 'Account.IndexController',
                controllerAs: 'vm',
                data: { activeTab: 'account' }
            });
    }

    function run($http, $rootScope, $window, UserService) {
        // add JWT token as default auth header
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + $window.jwtToken;

        // get current user
        UserService.GetCurrent().then(function (user) {
            $rootScope.curuser = user;
            $rootScope.userrole = user.role;
            if (user.useremail === "admin@icp.com") {
              $rootScope.userrole = 1;
            }
            $(".hi-activeuser").text("Welcome back, " + $rootScope.curuser.firstName + "!");
            if ($rootScope.userrole === 1)
            {
              //$(".dashboard-menu").show();
              document.location.href = '/dashboard';
            }
            $('#work-in-progress').fadeOut(500, function(){
            });
        }).then(function(){
        });

        // update active tab on state change
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.activeTab = toState.data.activeTab;
        });
    }

    // manually bootstrap angular after the JWT token is retrieved from the server
    $(function () {
        // get JWT token from server
        $.get('/app/token', function (token) {
            window.jwtToken = token;

            angular.bootstrap(document, ['app']);
        });
    });
})();
