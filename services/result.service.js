﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('results');
db.bind('tests');
db.bind('aquariums');
db.bind('users');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.getByTest = getByTest;
service.create = create;
service.update = update;
service.delete = _delete;
service.getByTestElement = getByTestElement;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
    db.collection("results").find({}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return result (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // result not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.results.findById(_id, function (err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return result (without hashed password)
            db.aquariums.findById(result.aquarium_id, function (err, aquarium){
              if (err) deferred.reject(err.name + ': ' + err.message);
              if (aquarium)
              {
                result.aquarium = aquarium;
                db.users.findById(result.aquarium.user_id, function (err, user){
                  if (err) deferred.reject(err.name + ': ' + err.message);
                  if (user)
                  {
                    result.user = user;
                    deferred.resolve(_.omit(result));
                  } else {
                    deferred.resolve();
                  }
                });
              } else {
                deferred.resolve();
              }
            });
        } else {
            // result not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByTestElement(test_id, element_id) {
    var deferred = Q.defer();
    db.results.findOne(
        { test_id, element_id },
        function (err, result) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (result) {
                deferred.resolve(_.omit(result));
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

async function getByTest(test_id) {
    var deferred = Q.defer();
    db.collection("results").find({test_id: test_id}).toArray(function(err, results) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (results) {

            if (results.length == 0)
            {
              deferred.resolve(_.omit(results));
            }

            var elem_cnt = 0;
            for(var i = 0; i < results.length; i++)
            {
              db.collection("elements").findById(results[i].element_id, function(err, element){
                elem_cnt++;
                if (element)
                {
                  for(var j = 0; j < results.length; j++)
                  {
                    if (results[j].element_id == element._id)
                    {
                      results[j].element = element;
                      break;
                    }
                  }
                  if (elem_cnt == results.length)
                  {
                    deferred.resolve(_.omit(results));
                  }
                }
              });
            }
        } else {
            // result not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(resultParam) {
    var deferred = Q.defer();

    function createResult() {
        var result = _.omit(resultParam);

        db.results.insert(
            result,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve(_.omit(doc));
            });
    }
    createResult(); // if need validation, cancel this line

    return deferred.promise;
}

function update(_id, resultParam) {
    var deferred = Q.defer();

    function updateResult() {
        // fields to update
        var set = { };
        if (resultParam.value != undefined)
          set.value = resultParam.value;

        db.results.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            { upsert: true},
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }
    updateResult(); // if need validation, cancel this line

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.results.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
