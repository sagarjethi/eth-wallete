﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('tests');
db.bind('aquariums');
db.bind('users');

const nodemailer = require("nodemailer");

var service = {};

service.getAll = getAll;
service.getById = getById;
service.getByAquarium = getByAquarium;
service.getByUnique = getByUnique;
service.create = create;
service.update = update;
service.updateUnique = updateUnique;
service.delete = _delete;
service.getLastTest = getLastTest;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
    db.collection("tests").find({}).sort({test_date: 1, test_time: 1}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return test (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // test not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.tests.findById(_id, function (err, test) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (test) {
            // return test (without hashed password)
            db.aquariums.findById(test.aquarium_id, function (err, aquarium){
              if (err) deferred.reject(err.name + ': ' + err.message);
              if (aquarium)
              {
                test.aquarium = aquarium;
                db.users.findById(test.aquarium.user_id, function (err, user){
                  if (err) deferred.reject(err.name + ': ' + err.message);
                  if (user)
                  {
                    test.user = user;
                    deferred.resolve(_.omit(test));
                  } else {
                    deferred.resolve();
                  }
                });
              } else {
                deferred.resolve();
              }
            });
        } else {
            // test not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByAquarium(aquarium_id) {
    var deferred = Q.defer();
    db.collection("tests").find({aquarium_id: aquarium_id}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return test (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // test not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getLastTest(aquarium_id) {
    var deferred = Q.defer();
    db.collection("tests").find({aquarium_id}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result && result.length > 0) {
            // return test (without hashed password)
            let last_id = 0;
            let last_date = result[0].test_date.substring(6, 10) + '-' + result[0].test_date.substring(0, 5);

            for(let i = 1; i < result.length; i++)
            {
                if (last_date < result[i].test_date.substring(6, 10) + '-' + result[i].test_date.substring(0, 5))
                {
                    last_id = i;
                    last_date = result[i].test_date.substring(6, 10) + '-' + result[i].test_date.substring(0, 5);
                }
            }
            deferred.resolve(_.omit(result[last_id]));
        } else {
            // test not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByUnique(unique_id) {
    var deferred = Q.defer();
    db.tests.findOne(
        { unique_id: unique_id.toString() },
        function (err, test) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (test) {
                deferred.resolve(_.omit(test));
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function create(testParam) {
    var deferred = Q.defer();

    function createTest() {
        var test = _.omit(testParam);

        db.tests.insert(
            test,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve(_.omit(doc));
            });
    }
    createTest(); // if need validation, cancel this line

    return deferred.promise;
}

function update(_id, testParam) {
    var deferred = Q.defer();

    function updateTest() {
        // fields to update
        var set = { };
        if (testParam.status != undefined)
          set.status = testParam.status;
        if (testParam.sent_date != undefined)
          set.sent_date = testParam.sent_date;
        if (testParam.sent_time != undefined)
          set.sent_time = testParam.sent_time;
        if (testParam.received_date != undefined)
          set.received_date = testParam.received_date;
        if (testParam.received_time != undefined)
          set.received_time = testParam.received_time;
        if (testParam.results_date != undefined)
          set.results_date = testParam.results_date;
        if (testParam.results_time != undefined)
          set.results_time = testParam.results_time;
        if (testParam.reminder_date != undefined)
          set.reminder_date = testParam.reminder_date;

        db.tests.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            { upsert: true},
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            }
        );
    }
    updateTest(); // if need validation, cancel this line

    return deferred.promise;
}

function updateUnique(_id, testParam) {
    var deferred = Q.defer();

    // validation
    db.tests.findOne(
        { unique_id: testParam.unique_id },
        function (err, test) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (test) {
                // test already exists
                deferred.reject('Unique ID "' + testParam.unique_id + '" is already taken');
            } else {
                updateTest();
            }
        });

    function updateTest() {
        // fields to update
        var set = {
            unique_id: testParam.unique_id
        };

        db.tests.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.tests.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
