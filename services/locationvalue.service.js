﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('locationvalues');

var service = {};

service.getAll = getAll;
service.getByLocation = getByLocation;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
    db.collection("locationvalues").find({}).sort({symbol: 1}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return location (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // location not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByLocation(location_id) {
    var deferred = Q.defer();
    var location_id = location_id.trim();
    db.locationvalues.find({location_id: location_id}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        if (result) {
            // return location (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // location not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.locationvalues.findById(_id, function (err, location) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (location) {
            // return location (without hashed password)
            deferred.resolve(_.omit(location));
        } else {
            // location not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(params) {
    var deferred = Q.defer();

    // validation
    db.locationvalues.findOne(
        { location_id: params.location_id, element_id: params.element_id },
        function (err, locationvalue) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (locationvalue) {
                deferred.resolve();
            } else {
                createLocationValue();
            }
        });

    function createLocationValue() {
        var locationvalue = _.omit(params);

        db.locationvalues.insert(
            locationvalue,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(location_id, element_id, value) {
    var deferred = Q.defer();
    var set = {
        value: value,
    };

    db.locationvalues.update(
        { location_id: location_id, element_id: element_id },
        { $set: set },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.locationvalues.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
