﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('notifications');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.getByFrequency = getByFrequency;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
// db.orders.find().sort( { amount: -1 } )
    db.collection("notifications").find({}).sort({symbol: 1}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return notification (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // notification not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.notifications.findById(_id, function (err, notification) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (notification) {
            // return notification (without hashed password)
            deferred.resolve(_.omit(notification));
        } else {
            // notification not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByFrequency(frequency) {
    var deferred = Q.defer();

    db.notifications.findOne(
        { frequency: frequency },
        function (err, notification) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (notification) {
                deferred.resolve(_.omit(notification));
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function create(notificationParam) {
    var deferred = Q.defer();

    // validation

    function createNotification() {
        var notification = _.omit(notificationParam);

        db.notifications.insert(
            notification,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    createNotification();

    return deferred.promise;
}

function update(_id, notificationParam) {
    var deferred = Q.defer();

    // validation

    function updateNotification() {
        // fields to update
        var set = {};
        if (notificationParam.type != undefined)
          set.type = notificationParam.type;
        if (notificationParam.name != undefined)
          set.name = notificationParam.name;
        if (notificationParam.frequency != undefined)
          set.frequency = notificationParam.frequency;
        if (notificationParam.status != undefined)
          set.status = notificationParam.status;
        if (notificationParam.subject != undefined)
          set.subject = notificationParam.subject;
        if (notificationParam.message != undefined)
          set.message = notificationParam.message;
        if (notificationParam.push != undefined)
          set.push = notificationParam.push;

        db.notifications.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    updateNotification();

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.notifications.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
