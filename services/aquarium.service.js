﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('aquariums');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.getByUser = getByUser;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
// db.orders.find().sort( { amount: -1 } )
    db.collection("aquariums").find({}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return aquarium (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // aquarium not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.aquariums.findById(_id, function (err, aquarium) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (aquarium) {
            // return aquarium (without hashed password)
            deferred.resolve(_.omit(aquarium));
        } else {
            // aquarium not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByUser(user_id) {
    var deferred = Q.defer();
    // db.aquariums.find(
    //     { user_id: user_id },
    //     function (err, aquariums) {
    //         if (err) deferred.reject(err.name + ': ' + err.message);
    //
    //         if (aquariums) {
    //             deferred.resolve(_.omit(aquariums));
    //         } else {
    //             deferred.resolve();
    //         }
    //     });
    db.collection("aquariums").find({user_id: user_id}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return aquarium (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // aquarium not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(aquariumParam) {
    var deferred = Q.defer();

    // validation
    // db.aquariums.findOne(
    //     { symbol: aquariumParam.symbol },
    //     function (err, aquarium) {
    //         if (err) deferred.reject(err.name + ': ' + err.message);
    //
    //         if (aquarium) {
    //             // aquarium already exists
    //             deferred.reject('Aquarium "' + aquariumsParam.symbol + '" is already taken');
    //         } else {
    //             createAquarium();
    //         }
    //     });

    function createAquarium() {
        var aquarium = _.omit(aquariumParam);

        db.aquariums.insert(
            aquarium,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve(_.omit(doc));
            });
    }
    createAquarium(); // if need validation, cancel this line

    return deferred.promise;
}

function update(_id, aquariumParam) {
    var deferred = Q.defer();

    // validation
    // db.aquariums.findById(_id, function (err, aquarium) {
    //     if (err) deferred.reject(err.name + ': ' + err.message);
    //
    //     if (aquarium.symbol !== aquariumParam.symbol) {
    //         // symbol has changed so check if the new aquarium is already taken
    //         db.aquariums.findOne(
    //             { symbol: aquariumParam.symbol },
    //             function (err, aquarium) {
    //                 if (err) deferred.reject(err.name + ': ' + err.message);
    //
    //                 if (aquarium) {
    //                     // aquarium already exists
    //                     deferred.reject('Email "' + aquariumParam.symbol + '" is already taken')
    //                 } else {
    //                     updateAquarium();
    //                 }
    //             });
    //     } else {
    //         updateAquarium();
    //     }
    // });

    function updateAquarium() {
        // fields to update
        var set = {
            // symbol: aquariumParam.symbol,
            // elemName: aquariumParam.elemName,
            // elemDesc: aquariumParam.elemDesc,
            // elemHigh: aquariumParam.elemHigh,
            // elemLow: aquariumParam.elemLow,
            // adviceLow: aquariumParam.adviceLow,
            // adviceHigh: aquariumParam.adviceHigh
        };
        if (aquariumParam.purchased != undefined)
          set.purchased = aquariumParam.purchased;

        db.aquariums.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }
    updateAquarium(); // if need validation, cancel this line

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.aquariums.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
