﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('elements');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.getBySymbol = getBySymbol;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
// db.orders.find().sort( { amount: -1 } )
    db.collection("elements").find({}).sort({symbol: 1}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return element (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // element not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.elements.findById(_id, function (err, element) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (element) {
            // return element (without hashed password)
            deferred.resolve(_.omit(element));
        } else {
            // element not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getBySymbol(symbol) {
    var deferred = Q.defer();

    db.elements.findOne(
        { symbol: symbol },
        function (err, element) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (element) {
                // element already exists
                deferred.resolve(_.omit(element));
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function create(elementParam) {
    var deferred = Q.defer();

    // validation
    db.elements.findOne(
        { symbol: elementParam.symbol },
        function (err, element) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (element) {
                // element already exists
                deferred.reject('Element "' + elementParam.symbol + '" is already taken');
            } else {
                createElement();
            }
        });

    function createElement() {
        var element = _.omit(elementParam);

        db.elements.insert(
            element,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve(_.omit(doc));
            });
    }

    return deferred.promise;
}

function update(_id, elementParam) {
    var deferred = Q.defer();

    // validation
    db.elements.findById(_id, function (err, element) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (element.symbol !== elementParam.symbol) {
            // symbol has changed so check if the new element is already taken
            db.elements.findOne(
                { symbol: elementParam.symbol },
                function (err, element) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (element) {
                        // element already exists
                        deferred.reject('Email "' + elementParam.symbol + '" is already taken')
                    } else {
                        updateElement();
                    }
                });
        } else {
            updateElement();
        }
    });

    function updateElement() {
        // fields to update
        var set = {
            symbol: elementParam.symbol,
            elemName: elementParam.elemName,
            elemDesc: elementParam.elemDesc,
            elemHigh: elementParam.elemHigh,
            elemLow: elementParam.elemLow,
            adviceLow: elementParam.adviceLow,
            adviceHigh: elementParam.adviceHigh
        };

        db.elements.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.elements.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
