﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('users');

var service = {};

service.authenticate = authenticate;
service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;
service.getByEmail = getByEmail;
service.getByToken = getByToken;
service.updateResetToken = updateResetToken;
service.updatePassword = updatePassword;

module.exports = service;

function authenticate(useremail, password) {
    var deferred = Q.defer();

    db.users.findOne({ useremail: useremail }, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user && bcrypt.compareSync(password, user.hash)) {
            // authentication successful
            deferred.resolve(jwt.sign({ sub: user._id }, config.secret));
        } else {
            // authentication failed
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getAll() {
    var deferred = Q.defer();

    db.collection("users").find({}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return user (without hashed password)
            deferred.resolve(_.omit(result, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByEmail(useremail) {
    var deferred = Q.defer();

    db.users.findOne(
        { useremail: useremail },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                deferred.resolve(_.omit(user, 'hash'));
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function getByToken(resetToken) {
    var deferred = Q.defer();

    db.users.findOne(
        { resetToken: resetToken },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                deferred.resolve(_.omit(user, 'hash'));
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function create(userParam) {
    var deferred = Q.defer();

    // validation
    db.users.findOne(
        { useremail: userParam.useremail },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                // useremail already exists
                deferred.reject('Email "' + userParam.useremail + '" is already taken');
            } else {
                createUser();
            }
        });

    function createUser() {
        // set user object to userParam without the cleartext password
        var user = _.omit(userParam, 'password');

        // add hashed password to user object
        user.hash = bcrypt.hashSync(userParam.password, 10);

        var now = new Date();
        var y = now.getFullYear();
        var m = now.getMonth();
        var d = now.getDate();
        if (m < 9)
          m = "0" + (m + 1);
        else
          m = m + 1;
        if (d < 10)
          d = "0" + d;
        user.created_at = y + "-" + m + "-" + d;

        db.users.insert(
            user,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(_id, userParam) {
    var deferred = Q.defer();

    // validation
    db.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user.useremail !== userParam.useremail) {
            // useremail has changed so check if the new useremail is already taken
            db.users.findOne(
                { useremail: userParam.useremail },
                function (err, user) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (user) {
                        // useremail already exists
                        deferred.reject('Email "' + userParam.useremail + '" is already taken')
                    } else {
                        updateUser();
                    }
                });
        } else {
            updateUser();
        }
    });

    function updateUser() {
        // fields to update
        var set = {
            firstName: userParam.firstName,
            lastName: userParam.lastName,
            useremail: userParam.useremail,
            role: userParam.role,
        };

        if (userParam.useremail == "admin@icp.com")
            set.role = 1;

        // update password if it was entered
        if (userParam.password) {
            set.hash = bcrypt.hashSync(userParam.password, 10);
        }

        db.users.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function updateResetToken(_id, resetToken) {
    var deferred = Q.defer();

    var set = {
        resetToken: resetToken,
    };

    db.users.update(
        { _id: mongo.helper.toObjectID(_id) },
        { $set: set },
        { upsert: true },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function updatePassword(_id, password) {
    var deferred = Q.defer();

    var set = {
        hash: bcrypt.hashSync(password, 10),
    };

    db.users.update(
        { _id: mongo.helper.toObjectID(_id) },
        { $set: set },
        { upsert: true },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.users.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
