﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('locations');
db.bind('locationvalues');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

function getAll() {
    var deferred = Q.defer();
    db.collection("locations").find({}).sort({symbol: 1}).toArray(function(err, result) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (result) {
            // return location (without hashed password)
            deferred.resolve(_.omit(result));
        } else {
            // location not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.locations.findById(_id, function (err, location) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (location) {
            // return location (without hashed password)
            deferred.resolve(_.omit(location));
        } else {
            // location not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(locationParam) {
    var deferred = Q.defer();

    // validation
    db.locations.findOne(
        { symbol: locationParam.name },
        function (err, location) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (location) {
                // location already exists
                deferred.reject('Location "' + locationParam.name + '" is already taken');
            } else {
                createLocation();
            }
        });

    function createLocation() {
        var location = _.omit(locationParam);

        db.locations.insert(
            location,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(_.omit(doc.insertedIds[0]));
            });
    }

    return deferred.promise;
}

function update(_id, locationParam) {
    var deferred = Q.defer();

    // validation
    db.locations.findById(_id, function (err, location) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (location.name !== locationParam.name) {
            // symbol has changed so check if the new location is already taken
            db.locations.findOne(
                { name: locationParam.name },
                function (err, location) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (location) {
                        // location already exists
                        deferred.reject('Location name "' + locationParam.name + '" is already taken')
                    } else {
                        updateLocation();
                    }
                });
        } else {
            updateLocation();
        }
    });

    function updateLocation() {
        // fields to update
        var set = {
            name: locationParam.name,
        };

        db.locations.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.locations.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
